FROM python
ARG MODULE_NAME='home/app'
WORKDIR "${MODULE_NAME}"

COPY . .

RUN pip install -r ./requirements.txt

CMD ["make" ,"local"]


